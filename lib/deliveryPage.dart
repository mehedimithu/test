import 'package:flutter/material.dart';

class deliveryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            "Your Order",
            style: TextStyle(
              fontSize: 28,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ), centerTitle: false,
          leading: IconButton(
            onPressed: () {},
            icon: Icon(Icons.close),
            color: Colors.green,
            iconSize: 30,
          ),
          actions: <Widget>[
            Text(
              "help",
              textAlign: TextAlign.right,
              style: TextStyle(fontSize: 25, color: Colors.green),
            )
          ],
        ),
        body: Stack(
          children: [
            Container(
              child: Column(
                children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Image.asset(
                              "assets/images/carrylogo.png",
                            ),
                          ),
                          margin: EdgeInsets.only(top: 0),
                        ),
                      ]),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Text(
                              "Thank you for the order! We are\n       processing your order",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 20,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          margin: EdgeInsets.only(top: 20),
                        ),
                      ]),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Text(
                              "Order Details",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 25,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          margin: EdgeInsets.only(top: 30, right: 220),
                        ),
                      ]),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Text(
                              "Your order                                  870 BDT",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 20,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          margin: EdgeInsets.only(top: 20, right: 20),
                        ),
                      ]),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Text(
                              "Your order number                   X234-DA",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 20,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          margin: EdgeInsets.only(top: 20, right: 20),
                        ),
                      ]),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Text(
                              "Delivery Address",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 25,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          margin: EdgeInsets.only(top: 20, right: 170),
                        ),
                      ]),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Text(
                              "Random Road STD,Dhaka",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 20,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          margin: EdgeInsets.only(top: 10, right: 145),
                        ),
                      ]),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Text(
                              "Sub Total                                     830 BDT",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          margin: EdgeInsets.only(top: 30, right: 10),
                        ),
                      ]),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Text(
                              "Delivery Charge                           40 BDT",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 20,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          margin: EdgeInsets.only(top: 10, right: 10),
                        ),
                      ]),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Text(
                              "-------------------------------------------------------",
                              style: TextStyle(
                                color: Colors.grey,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          margin: EdgeInsets.only(top: 10, right: 10),
                        ),
                      ]),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: Align(
                            alignment: Alignment.topCenter,
                            child: Text(
                              "Total                                              870 BDT",
                              style: TextStyle(
                                color: Colors.green,
                                fontSize: 20,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          margin: EdgeInsets.only(top: 10, right: 10),
                        ),
                      ]),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
