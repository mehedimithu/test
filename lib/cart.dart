import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:testit/customstepper.dart';

class Cart extends StatelessWidget {
  int qty = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Color(0xff00a3ff),
      appBar: AppBar(
        title: Text('cart'),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 20, left: 10, right: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.asset(
                  'assets/images/o.jpg',
                  fit: BoxFit.cover,
                ),
                Text(
                  'Deshi Onion',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.green),
                ),
                Text(
                  'blahblahblah\nsbgjbsdjkgbskjgbkdsj\ndjfnsjkfgsjdbgjsbgjbskGBKSBGKSBGbksGBK ',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                      color: Colors.grey),
                ),
              ],
            ),
          ),

          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                '460 BDT/Kg',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                    color: Colors.green),
              ),
              Text(
                'Delivery 39mins',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12,
                    color: Colors.grey),
              ),
            ],
          ),
          SizedBox(height: 6),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CustomStepper(
                  lowerLimit: 0,
                  upperLimit: 20,
                  stepValue: 1,
                  iconSize: 22.0,
                  value: this.qty,
                  onChanged: (value) {
                    print(value);
                  },
                ),
                SizedBox(
                  width: 20,
                ),
                RaisedButton(
                    color: Colors.green,
                    onPressed: () {},
                    child: Text(
                      'Add Cart',
                      style: TextStyle(color: Colors.white),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(8))),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'See relevant products',
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.green),
            ),
          ),
          Card(
            elevation: 10,
            margin: EdgeInsets.only(left: 16, right: 16, top: 10),
            child: Container(
              height: 130,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Colors.white,
              ),
              child: Column(
                children: [
                  ListTile(
                    leading: Image.asset(
                      'assets/images/p.png',
                      fit: BoxFit.fitHeight,
                    ),
                    title: Padding(
                      padding: EdgeInsets.all(5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Padma Elish',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17),
                          ),
                          Text(
                            'Delivery 39mins',
                            style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontSize: 12,
                                color: Colors.grey),
                          ),
                          SizedBox(height: 4),
                          Text(
                            '460 BDT/Kg',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 17,
                                color: Colors.green),
                          ),
                        ],
                      ),
                    ),
                    subtitle: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CustomStepper(
                          lowerLimit: 0,
                          upperLimit: 20,
                          stepValue: 1,
                          iconSize: 22.0,
                          value: this.qty,
                          onChanged: (value) {
                            print(value);
                          },
                        ),
                      ],
                    ),
                    trailing: RaisedButton(
                        color: Colors.green,
                        onPressed: () {},
                        child: Text(
                          'Add Cart',
                          style: TextStyle(color: Colors.white),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8))),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
