import 'package:flutter/material.dart';


class payout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {},
            icon: Icon(Icons.arrow_back_ios_sharp),
            color: Colors.green,
            iconSize: 34,
          ),
          title: Text("Payout",style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25,
              color: Colors.black
          ),
          ),
          centerTitle: false,
        ),
        body: Stack(
          children:<Widget>[ Column(
            children: <Widget>[

              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Text(
                          "Order  Summary",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 25,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(top: 30,right: 180),
                    ),
                  ]),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Text(
                          "Sub Total                                     830 BDT",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(top: 20, right: 10),
                    ),
                  ]),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Text(
                          "Delivery Charge                           40 BDT",
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 20,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(top: 10, right: 10),
                    ),
                  ]),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Text(
                          "Get Voucher?",
                          style: TextStyle(
                            color: Colors.green,
                            fontSize: 20,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.normal,

                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(top: 10,right: 255),
                    ),
                  ]),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Text(
                          "-------------------------------------------------------",
                          style: TextStyle(
                            color: Colors.grey,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(top: 10, right: 10),
                    ),
                  ]),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Text(
                          "Total                                              870 BDT",
                          style: TextStyle(
                            color: Colors.green,
                            fontSize: 20,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(top: 10, right: 10),
                    ),
                  ]),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Text(
                          "Other Options",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 23,
                            fontFamily: 'Roboto',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      margin: EdgeInsets.only(top: 20, right: 220,bottom: 20),
                    ),
                  ]),

              Row(mainAxisAlignment: MainAxisAlignment.center,

                children: <Widget>[
                  Row(mainAxisAlignment:MainAxisAlignment.center,
                    children: [Icon(
                      Icons.credit_card,
                      size: 20,),
                      Text("Pay with Credit/Debit Card            ",style: TextStyle(
                          color: Colors.grey,
                          fontSize: 20,
                          fontWeight: FontWeight.normal,
                          fontFamily: 'Roboto'),),
                      Radio(value: 'Credit', groupValue: null, onChanged: null),
                    ],
                  ),

                ],

                
              ),
              Row(mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                  Icons.bluetooth_audio_sharp,
                  size: 20,),
                  Text("Pay with bkash                               ",style: TextStyle(
                      color: Colors.grey,
                      fontSize: 20,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Roboto'),),
                  Radio(value: 'bkash', groupValue: null, onChanged: null),
                ],
              ),
              Row(mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                  Icons.keyboard_capslock_sharp,
                  size: 20,),
                  Text("Pay Cash                                         ",style: TextStyle(
                      color: Colors.grey,
                      fontSize: 20,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Roboto'),),
                  Radio(value: 'cash', groupValue: null, onChanged: null),
                ],
              ),Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    alignment: Alignment.topCenter,

                    child: RaisedButton(
                      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 80),onPressed: (){
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => payout()));

                    },color: Colors.green,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: Text(
                        "Place Order",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Roboto'),
                      ),
                    ),
                    margin: EdgeInsets.only(top: 20),
                  )
                ],
              ),


            ],
          ),
        ]),
      ),

    );
  }
}
