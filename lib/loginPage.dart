import 'package:flutter/material.dart';

class loginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      home: Scaffold(
        body: Stack(
          children: [
            Container(
              child:Column(
                  children: <Widget>[Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        alignment: Alignment.topCenter,
                        child: RaisedButton(
                          padding: EdgeInsets.symmetric(vertical: 18, horizontal: 10),onPressed: (){

                        },color: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(Radius.circular(20))),
                          child: Text(
                            "En",
                            style: TextStyle(
                                color: Colors.green,
                                fontSize: 14,
                                fontWeight: FontWeight.normal,
                                fontFamily: 'Roboto'),
                          ),
                        ),
                        margin: EdgeInsets.only(top: 60,right:280),
                      )
                    ],
                  ),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                      Container(
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Image.asset("assets/images/shopping.png",),
                        ),
                        margin: EdgeInsets.only(top: 50, left: 30),
                      ),
                    ]),

                    Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                      Container(
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Text("We will deliver your order\n      at your door step",
                            style: TextStyle(
                              color: Colors.green,
                              fontSize: 30,
                              fontFamily: 'Roboto',
                            ),),
                        ),
                        margin: EdgeInsets.only(top: 30, left: 30),
                      ),

                    ]),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(

                          alignment: Alignment.topCenter,
                          child: RaisedButton(
                            padding: EdgeInsets.symmetric(vertical: 30, horizontal: 120),onPressed: (){

                          },color: Colors.green,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(30))),
                            child: Text(
                              "Get Started",
                              style: TextStyle(
                                  backgroundColor: Colors.green,
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'Roboto'),
                            ),
                          ),
                          margin: EdgeInsets.only(top: 50),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.topCenter,
                          child: RaisedButton(
                            padding: EdgeInsets.symmetric(vertical: 30, horizontal: 130),onPressed: (){

                          },color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(30))),
                            child: Text(
                              "Sign up",
                              style: TextStyle(

                                  color: Colors.green,
                                  fontSize: 16,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'Roboto'),
                            ),
                          ),
                          margin: EdgeInsets.only(top: 20),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.topCenter,
                          child: RaisedButton.icon(
                            padding: EdgeInsets.symmetric(vertical: 30, horizontal: 66),onPressed: (){

                          },color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(30))),
                            label: Text(
                              "Sign up  with google",
                              style: TextStyle(
                                  color: Colors.green,
                                  fontSize: 16,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'Roboto'),
                            ),
                              icon: Icon(Icons.face,color: Colors.green,)

                          ),
                          margin: EdgeInsets.only(top: 20),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.topCenter,
                          child: RaisedButton.icon(
                            padding: EdgeInsets.symmetric(vertical: 30, horizontal: 60),onPressed: (){

                          },color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(30))),
                            label: Text(
                              "Sign up  with facebook",
                              style: TextStyle(
                                  color: Colors.green,
                                  fontSize: 16,
                                  fontWeight: FontWeight.normal,
                                  fontFamily: 'Roboto'),
                            ),
                            icon: Icon(Icons.face,color: Colors.green,),
                          ),
                          margin: EdgeInsets.only(top: 20),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          alignment: Alignment.topCenter,
                          child: Text(
                            "Forget Password?",
                            style: TextStyle(
                              color: Colors.green,
                              fontSize: 15,
                              fontFamily: 'Roboto',
                              decoration: TextDecoration.combine([
                                TextDecoration.underline,
                              ]),

                            ),

                          ),
                          margin: EdgeInsets.only(top: 60),
                        )
                      ],
                    ),
                  ]
              ),

            )
          ],

        ),
      ),

    );
  }
}
