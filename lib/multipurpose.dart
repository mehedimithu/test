import 'package:flutter/material.dart';
class multipurpose extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {},
            icon: Icon(Icons.menu),
            color: Colors.green,
            iconSize: 34,
          ),
          actions: <Widget>[
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.notifications_active_rounded),
              color: Colors.green,
              iconSize: 34,
            )
          ],
        ),
        body: Stack(
          children: [
            Container(
              child: Column(
                children: [
                  Row(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            height: 200,
                            width: 200,
                            child: Card(
                                semanticContainer: true,
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/images/food.png",
                                      fit: BoxFit.fill,
                                      height: 200,
                                      width: 200,
                                    ),
                                    Positioned(
                                      bottom: 16,
                                      child: Text(
                                        "Order Food",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 25,
                                            color: Colors.white),
                                      ),
                                    )
                                  ],
                                )),
                            margin: EdgeInsets.only(top: 15, left: 15),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            height: 200,
                            width: 200,
                            child: Card(
                                semanticContainer: true,
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/images/grs.png",
                                      fit: BoxFit.fill,
                                      height: 200,
                                      width: 200,
                                    ),
                                    Positioned(
                                      bottom: 16,
                                      child: Text(
                                        "Grocesssies",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 25,
                                            color: Colors.white),
                                      ),
                                    )
                                  ],
                                )),
                            margin: EdgeInsets.only(top: 15),
                          ),
                        ],
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            height: 150,
                            width: 135,
                            child: Card(
                                semanticContainer: true,
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                color: Colors.green,
                                shape: RoundedRectangleBorder(

                                  borderRadius: BorderRadius.circular(30),
                                ),
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Icon(
                                      Icons.ac_unit_rounded,
                                      color: Colors.white,
                                      size: 47,
                                    ),
                                    Positioned(
                                      bottom: 16,
                                      child: Text(
                                        "Fashion",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 25,
                                            color: Colors.white),
                                      ),
                                    )
                                  ],
                                )),
                            margin: EdgeInsets.only(top: 15, left: 15),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            height: 150,
                            width: 135,
                            child: Card(
                                semanticContainer: true,
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Icon(
                                      Icons.medical_services,
                                      color: Colors.green,
                                      size: 47,
                                    ),
                                    Positioned(
                                      bottom: 16,
                                      child: Text(
                                        "Medicine",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 25,
                                            color: Colors.green),
                                      ),
                                    )
                                  ],
                                )),
                            margin: EdgeInsets.only(top: 15),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            height: 150,
                            width: 135,
                            child: Card(
                                semanticContainer: true,
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Icon(
                                      Icons.home_sharp,
                                      color: Colors.green,
                                      size: 47,
                                    ),
                                    Positioned(
                                      bottom: 16,
                                      child: Text(
                                        "Hobbies",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 25,
                                            color: Colors.green),
                                      ),
                                    )
                                  ],
                                )),
                            margin: EdgeInsets.only(top: 15),
                          ),
                        ],
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Text(
                            "Services",
                            style: TextStyle(
                              color: Colors.green,
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        margin: EdgeInsets.only(top: 10, left: 20),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            height: 200,
                            width: 200,
                            child: Card(
                                semanticContainer: true,
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/images/iron.png",
                                      fit: BoxFit.fill,
                                      height: 200,
                                      width: 200,
                                    ),
                                    Positioned(
                                      bottom: 16,
                                      child: Text(
                                        "Laundry",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 25,
                                            color: Colors.white),
                                      ),
                                    )
                                  ],
                                )),
                            margin: EdgeInsets.only(top: 15, left: 15),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            height: 200,
                            width: 200,
                            child: Card(
                                semanticContainer: true,
                                clipBehavior: Clip.antiAliasWithSaveLayer,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Image.asset(
                                      "assets/images/hotel.png",
                                      fit: BoxFit.fill,
                                      height: 200,
                                      width: 200,
                                    ),
                                    Positioned(
                                      bottom: 16,
                                      child: Text(
                                        "Hotel Booking",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 25,
                                            color: Colors.white),
                                      ),
                                    )
                                  ],
                                )),
                            margin: EdgeInsets.only(top: 15),
                          ),
                        ],
                      )
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );;
  }
}
